﻿using System.Collections;
using System.Collections.Generic;
using BattleLogic;
using Environment;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerInterface : MonoBehaviour
{
    [SerializeField] private Battle battle;
    [SerializeField] private Player player;

    [SerializeField] private Text nicknameText;
    [SerializeField] private Text apText;
    [SerializeField] private Text timerText;
    [SerializeField] private Button closeSequenceButton;
    
    private void Awake()
    {
        battle = GameObject.Find("Battle").GetComponent<Battle>();
        player = GameObject.Find("PlayerPrefab(Clone)").GetComponent<Player>();
        nicknameText = GetComponentsInChildren<Text>()[0];
        apText = GetComponentsInChildren<Text>()[1];
        timerText = GetComponentsInChildren<Text>()[2];
        closeSequenceButton = GetComponentsInChildren<Button>()[0];
        closeSequenceButton.onClick.AddListener(battle.CloseSequence);
    }

    private void Update()
    {
        nicknameText.text = player.nickname + " [" + player.lvl + "]";
        apText.text = player.Ap + " AP";
        timerText.text = "00:" + battle.TickTimer;
    }
}
