﻿using System.Collections;
using System.Collections.Generic;
using BattleLogic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Environment;

public class HexGrid : MonoBehaviour {
    public static readonly List<AbstractSubject> Subjects = new List<AbstractSubject>();
    readonly List<AbstractSubject> subjects = new List<AbstractSubject>();
    public static readonly List<Cell> Cells = new List<Cell>();
    readonly List<Cell> cells = new List<Cell>();

    public static Player PlayerInstance
    {
        get
        {
            foreach (Player subject in Subjects)
            {
                if (subject is Player)
                {
                    return subject;
                }
            }
            return null;
        }
    }
    
    public int width = 6;
    public int height = 6;

    public Cell cellPrefab;
    public Player playerPrefab;
    public Monster monsterPrefab;
    public Text cellLabelPrefab;
    
    public GridMesh gridMesh;
    
    public Color defaultColor = Color.white;
	public Color hoverColor = Color.magenta;
	public Color damageColor = Color.red;
	public Color damageHoverColor = Color.grey;
    public Color playerColor = Color.blue;

    private Canvas gridCanvas;
    private string lastMousePosition = "";
    private Cell hoveredCell;
    private bool damage = false;

    void Awake() {
        gridCanvas = GetComponentInChildren<Canvas>();
        gridMesh = GetComponentInChildren<GridMesh>();

        for (int z = 0; z < height; z++) {
            for (int x = 0; x < width; x++) {
                Cell cell = CreateCell(x, z);
                if (z == 0 && x == 0)
                {
                    CreatePlayer(cell);
                }
                if (z == 3 && x == 3)
                {
                    CreateMonster(cell);
                }
            }
        }
    }

    void Start() {
        gridMesh.Triangulate(Cells);
    }

    void CreatePlayer(Cell cell)
    {
        Player player = Instantiate(playerPrefab);
        
        player.transform.SetParent(transform, true);
        player.transform.localPosition = cell.position;
        player.CurrentCell = cell;
        player.CurrentCell.subject = player;
        
        Subjects.Add(player);
        subjects.Add(player);
    }

    void CreateMonster(Cell cell)
    {
        Monster monster = Instantiate(monsterPrefab);
        
        monster.transform.SetParent(transform, true);
        monster.transform.localPosition = cell.position;
        monster.CurrentCell = cell;
        monster.CurrentCell.subject = monster;
        
        Subjects.Add(monster);
        subjects.Add(monster);
    }

    Cell CreateCell(int x, int z) {
        Vector3 position;
        position.x = (x + z * 0.5f - z / 2) * (HexagonMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = z * (HexagonMetrics.outerRadius * 1.5f);

        Cell cell = Instantiate(cellPrefab);
        Cells.Add(cell);
        cells.Add(cell);
        cell.position = position;
        cell.transform.SetParent(transform, false);
        cell.transform.localPosition = position;
        cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);

        Text label = cell.label = Instantiate<Text>(cellLabelPrefab);
        label.rectTransform.SetParent(gridCanvas.transform, false);
        label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
        label.text = cell.coordinates.ToStringOnSeparateLines();

        return cell;
    }
    
    void Update () {
        var speed = 20;
		if (Input.GetMouseButtonDown(0)) {
			HandleClick();
		}
		if (Input.GetMouseButtonDown(1)) {
		}

		if (Input.GetKeyDown(KeyCode.LeftShift)) {
		    foreach (Cell cell in Cells) {
		        cell.label.text = cell.MaxCoordinateOffset(PlayerInstance.CurrentCell).ToString();
		    }
        }
        if (Input.GetKeyUp(KeyCode.LeftShift)) {
		    foreach (Cell cell in Cells) {
                cell.label.text = cell.coordinates.ToStringOnSeparateLines();
		    }
        }

        if (Input.GetKeyDown(KeyCode.LeftControl)) {
		    damage = true;
            foreach (Cell cell in Cells) {
                if (cell.MaxCoordinateOffset(PlayerInstance.CurrentCell) == 1) {
                    cell.Damage = true;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftControl)) {
            damage = false;
            foreach (Cell cell in Cells) {
                if (cell.MaxCoordinateOffset(PlayerInstance.CurrentCell) == 1) {
                    cell.Damage = false;
                }
            }
        }

        if(Input.GetKey(KeyCode.RightArrow)) {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if(Input.GetKey(KeyCode.LeftArrow)) {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if(Input.GetKey(KeyCode.DownArrow)) {
            transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        }
        if(Input.GetKey(KeyCode.UpArrow)) {
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }
		if (lastMousePosition != Input.mousePosition.ToString()) {
            HandleMouseMove();
        }
	}

	void HandleMouseMove () {
        Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
		if (Physics.Raycast(inputRay, out hit)) {
            Cell cell = getCellByPosition(hit.point);
            if (cell && hoveredCell && !hoveredCell.Equals(cell))
                hoveredCell.Hover = false;
            hoveredCell = cell;
            hoveredCell.Hover = true;
            drawHexGrid();
        }
	}

	void HandleClick () {
		Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(inputRay, out hit)) {
            Cell cell = getCellByPosition(hit.point);
            if (cell && !damage) {
                PlayerInstance.Move(cell);
                drawHexGrid();
            } else {
                // 
            }
            // toggleCellColor(cell, defaultColor, touchedColor);
		}
	}

	Cell getCellByPosition(Vector3 position) {
        try
        {
            position = transform.InverseTransformPoint(position);
            HexCoordinates coordinates = HexCoordinates.FromPosition(position);
            int index = coordinates.X + coordinates.Z * width + coordinates.Z / 2;
            return Cells[index];
        }
        catch
        {
            return null;
        }
    }
	
	void drawHexGrid () {
		gridMesh.Triangulate(cells);
    }
}
