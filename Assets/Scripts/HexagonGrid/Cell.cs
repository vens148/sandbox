﻿using System.Collections;
using System;
using System.Collections.Generic;
using Environment;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public struct HexCoordinates {
    [SerializeField]
    private int x, z;

    public int X
    {
        get
        {
            return x;
        }
    }
    public int Y
    {
        get
        {
            return -X - Z;
        }
    }
    public int Z
    {
        get
        {
            return z;
        }
    }

    public HexCoordinates(int x, int z)
    {
        this.x = x;
        this.z = z;
    }
    
    public static HexCoordinates FromPosition (Vector3 position) {
		float x = position.x / (HexagonMetrics.innerRadius * 2f);
		float y = -x;
        
        float offset = position.z / (HexagonMetrics.outerRadius * 3f);
		x -= offset;
		y -= offset;
        
        int iX = Mathf.RoundToInt(x);
		int iY = Mathf.RoundToInt(y);
		int iZ = Mathf.RoundToInt(-x -y);
        
        if (iX + iY + iZ != 0) {
			float dX = Mathf.Abs(x - iX);
			float dY = Mathf.Abs(y - iY);
			float dZ = Mathf.Abs(-x -y - iZ);

			if (dX > dY && dX > dZ) {
				iX = -iY - iZ;
			}
			else if (dZ > dY) {
				iZ = -iX - iY;
			}
		}

		return new HexCoordinates(iX, iZ);
	}


    public override string ToString()
    {
        return "(" +
            X.ToString() + ", " + Y.ToString() + ", " + Z.ToString() + ")";
    }

    public string ToStringOnSeparateLines()
    {
        return X.ToString() + "\n" + Z.ToString();
    }

    public static HexCoordinates FromOffsetCoordinates(int x, int z)
    {
        return new HexCoordinates(x - z / 2, z);
    }

}
public class Cell : MonoBehaviour
{
    public Color defaultColor;
    public Color hoverColor;
    public Color damageColor;
    public Color damageHoverColor;
    public Color playerColor;
    public Color monsterColor;
    
    public Text label;
    public Vector3 position;
    public HexCoordinates coordinates;
	private Color color;
    public AbstractSubject subject = null;
    private bool damage;
    
    [SerializeField] private Cell[] neighbors;
    
    public bool Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    private bool hover;
    public bool Hover
    {
        get { return hover; }
        set { hover = value; }
    }

    public Color Color
    {
        get
        {
            if (subject)
                if (subject is Player)
                    return playerColor;
                else if (subject is Monster)
                    return monsterColor;
            if (damage)
            {
                return hover ? damageHoverColor : damageColor;
            }

            return hover ? hoverColor : defaultColor;
        }
    }

    public int MaxCoordinateOffset(Cell cell) {

        int offsetX = Math.Abs(coordinates.X - cell.coordinates.X);
        int offsetY = Math.Abs(coordinates.Y - cell.coordinates.Y);
        int offsetZ = Math.Abs(coordinates.Z - cell.coordinates.Z);

        int max = Math.Max(offsetX, Math.Max(offsetY, offsetZ));

        return max;
	}
    
    public Cell() {}
}
