﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class GridMesh : MonoBehaviour
{

    Mesh hexMesh;
    List<Vector3> vertices;
    List<int> triangles;
    MeshCollider meshCollider;
    List<Color> colors;

    void Awake() {
        GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
		meshCollider = gameObject.AddComponent<MeshCollider>();
        hexMesh.name = "Hex Mesh";
        vertices = new List<Vector3>();
		colors = new List<Color>();
        triangles = new List<int>();
    }

    public void Triangulate(List<Cell> cells) {
        hexMesh.Clear();
        vertices.Clear();
		colors.Clear();
        triangles.Clear();
        foreach (Cell cell in cells) {
            Triangulate(cell);
        }
        hexMesh.vertices = vertices.ToArray();
		hexMesh.colors = colors.ToArray();
        hexMesh.triangles = triangles.ToArray();
        hexMesh.RecalculateNormals();
		meshCollider.sharedMesh = hexMesh;
    }

    void Triangulate(Cell cell) {
        Vector3 center = cell.transform.localPosition;
        for (int i = 0; i < 6; i++)
        {
            AddTriangle(
                center,
                center + HexagonMetrics.corners[i],
                center + HexagonMetrics.corners[i + 1]
            );
			AddTriangleColor(cell.Color);
        }
    }

	void AddTriangleColor (Color color) {
		colors.Add(color);
		colors.Add(color);
		colors.Add(color);
	}

    void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3) {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }
}
