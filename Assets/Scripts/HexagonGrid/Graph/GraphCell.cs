namespace HexagonGrid.Graph
{
    public class GraphCell
    {
        public GraphCell topRight = null;
        public GraphCell right = null;
        public GraphCell bottomRight = null;
        public GraphCell bottomLeft = null;
        public GraphCell left = null;
        public GraphCell topLeft = null;
    }
}