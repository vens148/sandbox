using System.Collections;
using System.Collections.Generic;
using Environment;
using UnityEngine;

namespace BattleLogic
{
    public class Battle : MonoBehaviour {
        public const int SEQUENCE_TIME = 45;
        public bool IsSequence = true;
        
        public static List<List<IAction>> sequences = new List<List<IAction>>();
        public static List<IAction> actions = new List<IAction>();
        
        public static void Move (Cell from, Cell to, int ap, AbstractSubject subject) {
            actions.Add(new Move(from, to, ap, subject));
        }
        public static void Next (AbstractSubject subject) {
            actions.Add(new Next(subject));
        }

        private float tick = 0;
        [SerializeField] private int tickTimer = SEQUENCE_TIME;
        public int TickTimer
        {
            get { return tickTimer; }
            set
            {
                if (value <= 0)
                {
                    CloseSequence();
                }
                else
                {
                    tickTimer = value;
                }
            }
        }
        private void Update()
        {
            if (IsSequence)
            {
                tick += Time.deltaTime;
                if (tick >= 1)
                {
                    TickTimer = TickTimer - 1;
                    tick = tick - 1;
                }
            }
        }
        public void CloseSequence()
        {
            Debug.Log("Close Sequense");
            IsSequence = false;
            
            // Многозначительный 5-секундный лоадер
            // System.Threading.Thread.Sleep(5000);
         
            Player player = GameObject.Find("PlayerPrefab(Clone)").GetComponent<Player>();
            player.SetMaxAp();
            tickTimer = SEQUENCE_TIME;
            IsSequence = true;
            // foreach (IAction action in actions)
            // {
            //     if (action.Type == ActionType.NEXT)
            //     {
            //         action.Subject.CurrentCell = action.Subject.CurrentCell;
            //     }
            // }
            // sequences.Add(actions);
            // actions = new List<IAction>();
            // tickTimer = SEQUENCE_TIME;
        }
    }
}