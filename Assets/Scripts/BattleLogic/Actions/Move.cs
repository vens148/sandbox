using Environment;

namespace BattleLogic
{
    public class Move : IAction 
    {
        private readonly ActionType type = ActionType.MOVE;
        public ActionType Type
        {
            get { return type; }
        }
    
        private AbstractSubject subject;
        public AbstractSubject Subject
        {
            get { return subject; }
        }
    
        public readonly Cell from;
        public readonly Cell to;
    
        public Move(Cell from, Cell to, int ap, AbstractSubject subject) {
            type = ActionType.MOVE;
            this.from = from;
            this.to = to;
            this.subject = subject;
        }

        public int AP;
    
        public override string ToString() {
            return "Move " + "(" + AP + ")" + ": " + from.coordinates.ToString() + " => " + to.coordinates.ToString();
        }
    }
}