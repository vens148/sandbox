using Environment;

namespace BattleLogic
{
    public class Next : IAction
    {
        private readonly ActionType type = ActionType.NEXT;
        public ActionType Type
        {
            get { return type; }
        }
    
        private AbstractSubject subject;
        public AbstractSubject Subject
        {
            get { return subject; }
        }
    
        public Next(AbstractSubject subject) {
            type = ActionType.NEXT;
            this.subject = subject;
        }
    
        public int AP {
            get { return 0; }
        }
    
        public override string ToString() {
            return "Next step";
        }
    }
}