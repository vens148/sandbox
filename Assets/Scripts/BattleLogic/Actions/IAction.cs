﻿using Environment;

namespace BattleLogic
{
    public enum ActionType: byte
    {
        MOVE = 1,
        NEXT = 2
    }
    
    public interface IAction
    {
        ActionType Type { get; }
        AbstractSubject Subject { get; }
    }
}