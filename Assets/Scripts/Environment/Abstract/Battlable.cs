using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Environment
{
    public abstract class Battlable : MonoBehaviour, IBattlable 
    {
        private GridMesh mesh;
        
        public abstract Cell CurrentCell { get; set; }
    }
}