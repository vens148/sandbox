namespace Environment
{
    public interface IBattlable
    {
        Cell CurrentCell { get; set; }
    }
}