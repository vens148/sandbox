using System.Collections.Generic;
using BattleLogic;
using Environment.Mixins;
using UnityEngine;
namespace Environment
{
    public class Player : AbstractSubject
    {
        public Animator anim;
        public string WalkingTriggerName;

        public string nickname;
        public byte lvl = 8;

        [SerializeField] private int MAX_HP = 20;
        [SerializeField] private int MAX_AP = 10;
        [SerializeField] private bool isWalking;
        [SerializeField] public int Hp;
        public int Ap;
        protected int rotation = 90;
        protected bool moving = false;
        protected float speed = 0.02f;
        
        private Vector3 currentPosition;
        private Vector3 targetPosition;

        protected List<Cell> currentMoveTraectory = new List<Cell>();
        int traectoryIterate = 0;
        private int WalkingHash;
        protected virtual void Start()
        {
            currentPosition = CurrentCell.position;
            targetPosition = CurrentCell.position;
            Hp = MAX_HP;
            Ap = MAX_AP;
            //anim = GetComponent<Animator>();
            WalkingHash = Animator.StringToHash(WalkingTriggerName);
            anim.SetBool(WalkingHash,isWalking);
        }
        protected void Update()
        {
            
            anim.SetBool(WalkingHash, isWalking);
        }
        protected void FixedUpdate()
        {
            var positionDelta = targetPosition - transform.position;
            var access = Mathf.Abs(positionDelta.x) < speed * 2 &&
                         Mathf.Abs(positionDelta.y) < speed * 2 &&
                         Mathf.Abs(positionDelta.z) < speed * 2;
            if (!access)
            {
                Vector3 rotationVector = new Vector3(
                    transform.rotation.x,
                    getRotation(
                        currentMoveTraectory[traectoryIterate],
                        currentMoveTraectory[traectoryIterate + 1]
                    ),
                    transform.rotation.z
                );
                isWalking = true;
                Vector3 traectoryDelta = (targetPosition - currentPosition) * speed;
                transform.position += traectoryDelta;
            }
            else
            {
                if (currentMoveTraectory.Count > traectoryIterate + 2)
                {
                    ++traectoryIterate;
                    Vector3 rotationVector = new Vector3(
                        transform.rotation.x,
                        getRotation(
                            currentMoveTraectory[traectoryIterate],
                            currentMoveTraectory[traectoryIterate + 1]
                        ),
                        transform.rotation.z
                    );
                    transform.rotation = Quaternion.Euler(rotationVector);
                    currentPosition = currentMoveTraectory[traectoryIterate].position;
                    targetPosition = currentMoveTraectory[traectoryIterate + 1].position;
                }
                else
                {
                    isWalking = false;
                }
            }
        }
        public override void Move(Cell cell)
        {
            int moveAp = CalculateApForMove(cell);
            if (Ap >= moveAp && !isWalking)
            {
                currentMoveTraectory = GetWayTraectory(CurrentCell, cell);

                CurrentCell.subject = null;
                CurrentCell = cell;
                CurrentCell.subject = this;
                traectoryIterate = 0;

                if (currentMoveTraectory.Count > traectoryIterate + 1)
                {
                    Ap -= moveAp;
                    Vector3 rotationVector = new Vector3(
                        transform.rotation.x,
                        getRotation(
                            currentMoveTraectory[traectoryIterate],
                            currentMoveTraectory[traectoryIterate + 1]
                        ),
                        transform.rotation.z
                    );
                    transform.rotation = Quaternion.Euler(rotationVector);
                    currentPosition = currentMoveTraectory[traectoryIterate].position;
                    targetPosition = currentMoveTraectory[traectoryIterate + 1].position;
                }
            }
        }
        public void SetMaxAp() { Ap = MAX_AP; }
    }
    
    
}