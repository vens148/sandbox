using System.Collections.Generic;
using BattleLogic;
using UnityEngine;

namespace Environment
{
    public abstract class AbstractSubject : Battlable
    {
        protected Cell currentCell = null;

        public override Cell CurrentCell
        {
            get
            {
                Cell v = null;
                foreach (Move action in Battle.actions)
                {
                    if (action.Subject.Equals(this))
                    {
                        v = action.to;
                    }
                }
                return v ?? currentCell;
            }
            set
            {
                currentCell = value;
            }
        }

        // TODO полиморфить
        public int CalculateApForMove(Cell cell)
        {
            return cell.MaxCoordinateOffset(CurrentCell);
        }
        
        // TODO переделать в граф
        protected List<Cell> GetWayTraectory(Cell cell1, Cell cell2)
        {
            List<Cell> result = new List<Cell>();
            HexGrid grid = GetComponentInParent<HexGrid>();
            HexCoordinates walkingCoordinates = cell1.coordinates;
            Vector3 walkingPosition = cell1.position;
            
            result.Add(cell1);
            while (!walkingCoordinates.Equals(cell2.coordinates))
            {
                int x = walkingCoordinates.X;
                int z = walkingCoordinates.Z;

                if (x > cell2.coordinates.X || x < cell2.coordinates.X)
                {
                    if (x < cell2.coordinates.X) x++;
                    if (x > cell2.coordinates.X) x--;
                }
                else if (z > cell2.coordinates.Z || z < cell2.coordinates.Z)
                {
                    if (z < cell2.coordinates.Z) z++;
                    if (z > cell2.coordinates.Z) z--;
                }

                HexCoordinates newCoordinates = new HexCoordinates(x, z);
                Cell searchingCell = null;
                foreach (Cell cell in HexGrid.Cells)
                {
                    if (cell.coordinates.Equals(newCoordinates))
                    {
                        searchingCell = cell;
                        break;
                    }
                }
                
                walkingCoordinates = searchingCell.coordinates;
                walkingPosition = searchingCell.position;
                result.Add(searchingCell);
            }

            return result;
        }

        protected int getRotation(Cell cell1, Cell cell2)
        {
            if (cell1.coordinates.X - cell2.coordinates.X == 0 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == -1)
                return 30;
            if (cell1.coordinates.X - cell2.coordinates.X == -1 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == 0)
                return 90;
            if (cell1.coordinates.X - cell2.coordinates.X == -1 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == 1)
                return 150;
            if (cell1.coordinates.X - cell2.coordinates.X == 0 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == 1)
                return 210;
            if (cell1.coordinates.X - cell2.coordinates.X == 1 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == 0)
                return 270;
            if (cell1.coordinates.X - cell2.coordinates.X == 1 &&
                Mathf.Abs(cell1.coordinates.Y - cell2.coordinates.Y) <= 1 &&
                cell1.coordinates.Z - cell2.coordinates.Z == -1)
                return 330;
            return 0;
        }

        public virtual void Move(Cell cell)
        {
            int moveAp = CalculateApForMove(cell);
            List<Cell> traectory = GetWayTraectory(CurrentCell, cell);
            CurrentCell.subject = null;
            Battle.Move(CurrentCell, cell, this);
            CurrentCell = cell;
            CurrentCell.subject = this;
            transform.position = cell.transform.position;
        }
    }
}