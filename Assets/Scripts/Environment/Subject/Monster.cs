using Environment.Mixins;

namespace Environment
{
    public class Monster : AbstractSubject, IHealthable
    {
        public const int MAX_HP = 30;

        private int hp = MAX_HP;
        public int Hp
        {
            get { return hp; }
            set { hp = value; }
        }
    }
}