namespace Environment.Mixins
{
    public interface IHealthable
    {
        int Hp { get; set; }
    }
}